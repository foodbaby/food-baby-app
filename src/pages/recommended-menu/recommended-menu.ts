import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

@Component({
  selector: 'page-recommended-menu',
  templateUrl: 'recommended-menu.html',
})
export class RecommendedMenuPage {

  constructor(public navCtrl: NavController) {
  }

}
