import {
  Component,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  ChangeDetectorRef, Renderer
} from '@angular/core';

import { NgForm } from '@angular/forms';

import {NavController, NavParams, ViewController} from 'ionic-angular';

import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import * as Constants from '../../globals/constants';
import {Storage} from "@ionic/storage";
import {stripe, elements} from "../../app/app.component";

@Component({
  selector: 'page-checkout-modal',
  templateUrl: 'checkout-modal.html'
})
export class CheckoutModalPage implements AfterViewInit, OnDestroy {

  public selections: any[] = this.navParams.get('selections');
  public restaurantIndex: number = this.navParams.get('restaurantIndex');

  public orders: any[] = [[],[],[],[],[]];
  public customer: string;

  public email: string = "";

  public isPostingCharge: boolean = false;

  // STRIPE
  @ViewChild('cardInfo') cardInfo: ElementRef;

  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              private storage: Storage, private http: HttpClient, private cd: ChangeDetectorRef,
              private renderer: Renderer) {

    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'checkoutModal', true);

    storage.get('orders').then((val) => {
      if(null != val && val.length > 0) {
        this.orders = JSON.parse(val);
      }
    });

    storage.get('customer').then((val) => {
      if(null != val && val.length > 0) {
        this.customer = val;
      }
    });
  }

  closeModal(isSuccessfulOrder) {
    this.viewCtrl.dismiss({ "isSuccessfulOrder": isSuccessfulOrder });
  }

  getTotalPrice() {
    let total = 0.00;

    for(let i = 0; i < this.selections[this.restaurantIndex].length; i++) {
      let selection = this.selections[this.restaurantIndex][i];
      total += (selection.price * selection.quantity);
    }

    return total.toFixed(2);
  }

  getItemsString() {
    let returnString = "";

    for(let i = 0; i < this.selections[this.restaurantIndex].length; i++) {
      let selection = this.selections[this.restaurantIndex][i];
      returnString += selection.name;

      if(i < this.selections[this.restaurantIndex].length - 1) {
        returnString += ", ";
      }
    }

    return returnString;
  }

  ngAfterViewInit() {
    const style = {
      base: {
        lineHeight: '24px',
        fontFamily: 'monospace',
        fontSmoothing: 'antialiased',
        fontSize: '19px',
        '::placeholder': {
          color: '#0094fe'
        }
      }
    };

    this.card = elements.create('card', { style });
    this.card.mount(this.cardInfo.nativeElement);

    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      this.postCustomer(token);
    }
  }

  postCustomer(token: any) {
    let headers = new HttpHeaders();
    headers = headers.set("Accept", 'application/json').set('Content-Type', 'application/x-www-form-urlencoded');

    const body = new HttpParams()
      .set('email', this.email)
      .set('stripeToken', token.id);

    this.http.post(Constants.API_URL + "/stripe/customer", body, {headers: headers})
      .subscribe(data => {
        let customer = data['Customer'];
        this.storage.set('customer', customer.id);
        this.customer = customer.id;

        this.postCharge(customer.id);
      }, error => {
        console.log(error);// Error getting the data
      });
  }

  postCharge(customerID: any) {
    if(!this.isPostingCharge) {
      this.isPostingCharge = true;

      let headers = new HttpHeaders();
      headers = headers.set("Accept", 'application/json').set('Content-Type', 'application/x-www-form-urlencoded');

      let price = +this.getTotalPrice();

      price = price * 100;


      const body = new HttpParams()
        .set('amount', price.toString())
        .set('description', this.getItemsString())
        .set('stripeCustomer', customerID);

      this.http.post(Constants.API_URL + "/stripe/charge", body, {headers: headers})
        .subscribe(data => {
          let charge = data['Charge'];

          for(let i  = 0; i < this.selections[this.restaurantIndex].length; i++) {
            this.orders[this.restaurantIndex].push(this.selections[this.restaurantIndex][i]);
          }

          this.storage.set('orders', JSON.stringify(this.orders));

          this.selections[this.restaurantIndex] = [];

          this.storage.set('foodSelections', JSON.stringify(this.selections));
          this.isPostingCharge = false;

          this.closeModal(true);

        }, error => {
          console.log(error);// Error getting the data
          this.isPostingCharge = false;
        });
    }
  }

}
