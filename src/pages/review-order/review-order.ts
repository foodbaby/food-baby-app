import { Component} from '@angular/core';

import { NgForm } from '@angular/forms';

import {ModalController, NavController, NavParams} from 'ionic-angular';

import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import * as Constants from '../../globals/constants';
import {Storage} from "@ionic/storage";
import {FoodModalPage} from "../food-modal/food-modal";
import {CheckoutModalPage} from "../checkout-modal/checkout-modal";

@Component({
  selector: 'page-review-order',
  templateUrl: 'review-order.html',
})
export class ReviewOrderPage {

  public restaurant: any = this.navParams.get('restaurant');
  public restaurantIndex: number = this.navParams.get('restaurantIndex');
  public selections: any[] = this.navParams.get('selections');
  public orders: any[] = [[],[],[],[],[]];

  public isPaying: boolean = true;


  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
              private http: HttpClient, private modalCtrl: ModalController) {

    storage.get('orders').then((val) => {
      if(null != val && val.length > 0) {
        this.orders = JSON.parse(val);
      }
    });
  }

  getTotalPrice() {
    let total = 0.00;

    for(let i = 0; i < this.selections[this.restaurantIndex].length; i++) {
      let selection = this.selections[this.restaurantIndex][i];
      total += (selection.price * selection.quantity);
    }

    return total.toFixed(2);
  }

  getItemsString() {
    let returnString = "";

    for(let i = 0; i < this.selections[this.restaurantIndex].length; i++) {
      let selection = this.selections[this.restaurantIndex][i];
      returnString += selection.name;

      if(i < this.selections[this.restaurantIndex].length - 1) {
        returnString += ", ";
      }
    }

    return returnString;
  }

  openCheckoutModal() {
    let modal = this.modalCtrl.create(CheckoutModalPage,
      {"selections": this.selections, "restaurantIndex": this.restaurantIndex},
      {showBackdrop: true, enableBackdropDismiss: true});

    modal.onDidDismiss(data => {
      if(null != data && data.isSuccessfulOrder) {
        this.toggleIsPaying(false);
      }
    });

    modal.present();
  }

  toggleIsPaying(newVal) {
    this.isPaying = newVal;

    this.storage.get('orders').then((val) => {
      if(null != val && val.length > 0) {
        this.orders = JSON.parse(val);
      }
    });
  }
}
