import { Component } from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'page-restaurant',
  templateUrl: 'restaurant.html'
})
export class RestaurantPage {

  public foods: any[] = this.navParams.get('foods');
  public image_url: string = this.navParams.get('image_url');
  public facebook_url: string = this.navParams.get('facebook_url');
  public instagram_url: string = this.navParams.get('instagram_url');
  public name: string = this.navParams.get('name');
  public description: string = this.navParams.get('description');
  public video_url: string = this.navParams.get('video_url');


  constructor(public navCtrl: NavController, public navParams: NavParams, private domSanitizer: DomSanitizer) {
  }

  getSafeVideoUrl() {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(this.video_url);
  }
}
