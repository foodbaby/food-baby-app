import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';

import * as Constants from "../../globals/constants";
import {FoodModalPage} from "../food-modal/food-modal";

@Component({
  selector: 'page-tour-gallery',
  templateUrl: 'tour-gallery.html'
})
export class TourGalleryPage {

  public tour: any = Constants.TOUR;
  public foods: any[] = [];

  constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

    for(let i = 0; i < this.tour.restaurants.length; i++) {
      let restaurant = this.tour.restaurants[i];

      for(let j = 0; j < restaurant.foods.length; j++) {
        this.foods.push(restaurant.foods[j]);
      }
    }

  }

  openFoodModal(food) {
    let modal = this.modalCtrl.create(FoodModalPage, food, {showBackdrop: true, enableBackdropDismiss: true});
    modal.present();
  }
}
