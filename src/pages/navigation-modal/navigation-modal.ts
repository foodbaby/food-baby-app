import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {LaunchNavigator, LaunchNavigatorOptions} from "@ionic-native/launch-navigator";

@Component({
  selector: 'page-navigation-modal',
  templateUrl: 'navigation-modal.html'
})
export class NavigationModalPage {

  public restaurants: any = this.navParams.get('restaurants');

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private launchNavigator: LaunchNavigator) {
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  getRouteString(address: any) {
    return address.address1 + " " + address.locality + ", " + address.region + " " + address.zipcode;
  }


  navigateToRestaurant(restaurant: any) {
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
      transportMode: this.launchNavigator.TRANSPORT_MODE.WALKING
    };

    let routeString = this.getRouteString(restaurant.address);

    this.launchNavigator.navigate(routeString, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }
}
