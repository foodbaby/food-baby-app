import { Component } from '@angular/core';
import {NavController, Tabs} from 'ionic-angular';
import { MoreInformationPage } from "../more-information/more-information";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

import * as Constants from "../../globals/constants";
import {TourGalleryPage} from "../tour-gallery/tour-gallery";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public tour: any = Constants.TOUR;

  constructor(public navCtrl: NavController, private launchNavigator: LaunchNavigator, private tabs: Tabs) {
  }

  getStarIconName(index) {
    if(this.tour.rating >= (index * 2)) {
      return "star";
    }
    else if(this.tour.rating == (index * 2 - 1)) {
      return "star-half";
    }
    else {
      return "star-outline";
    }
  }

  getTourTags() {
    let returnString = "";

    for(let i = 0; i < this.tour.tags.length; i++) {
      returnString += this.tour.tags[i];

      if(i != this.tour.tags.length - 1) {
        returnString += ", ";
      }
    }

    return returnString;
  }

  goToTourGallery() {
    this.navCtrl.push(TourGalleryPage);
  }

  goToMoreInfo() {
    this.navCtrl.push(MoreInformationPage);
  }

  goToStartTour() {
    this.tabs.select(2);
  }

  getRouteString() {
    let returnString = "";

    for(let i = 0; i < this.tour.restaurants.length; i++) {
      let address = this.tour.restaurants[i].address;

      returnString += address.address1 + " " + address.locality + ", " + address.region + " " + address.zipcode;
      if(i < this.tour.restaurants.length - 1) {
        returnString += "+to:";
      }
    }

    return returnString;
  }

  openGoogleMaps() {
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
      transportMode: this.launchNavigator.TRANSPORT_MODE.WALKING
    };

    let routeString = this.getRouteString();

    this.launchNavigator.navigate(routeString, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }
}
