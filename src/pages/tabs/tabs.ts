import { Component } from '@angular/core';
import {ComingSoonPage} from "../coming-soon/coming-soon";
import { HomePage } from '../home/home';
import {StartTourPage} from "../start-tour/start-tour";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = StartTourPage;
  tab3Root = ComingSoonPage;

  constructor() {

  }
}
