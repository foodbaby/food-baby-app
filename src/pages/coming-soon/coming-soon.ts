import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {LaunchNavigator, LaunchNavigatorOptions} from "@ionic-native/launch-navigator";
import * as Constants from "../../globals/constants";

@Component({
  selector: 'page-coming-soon',
  templateUrl: 'coming-soon.html'
})
export class ComingSoonPage {

  public tour: any = Constants.TOUR;

  constructor(public navCtrl: NavController, public launchNavigator: LaunchNavigator) {}

  getRouteString() {
    let returnString = "";

    for(let i = 0; i < this.tour.restaurants.length; i++) {
      let address = this.tour.restaurants[i].address;

      returnString += address.address1 + " " + address.locality + ", " + address.region + " " + address.zipcode;
      if(i < this.tour.restaurants.length - 1) {
        returnString += "+to:";
      }
    }

    return returnString;
  }

  openGoogleMaps() {
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
      transportMode: this.launchNavigator.TRANSPORT_MODE.WALKING
    };

    let routeString = this.getRouteString();

    this.launchNavigator.navigate(routeString, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }
}
