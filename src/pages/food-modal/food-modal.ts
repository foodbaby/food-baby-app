import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';

@Component({
  selector: 'page-food-modal',
  templateUrl: 'food-modal.html'
})
export class FoodModalPage {

  public image_url: string = this.navParams.get('image_url');
  public name: string = this.navParams.get('name');
  public description: string = this.navParams.get('description');

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }
}
