import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

import * as Constants from '../../globals/constants';
import {Storage} from "@ionic/storage";
import {ReviewOrderPage} from "../review-order/review-order";

@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html',
})
export class PayPage {

  public restaurants: any[] = Constants.TOUR.restaurants;
  public selections: any[] = [];

  constructor(public navCtrl: NavController, private storage: Storage) {
    storage.get('foodSelections').then((val) => {
      if(null != val && val.length > 0) {
        this.selections = JSON.parse(val);
      }

      for(let i = 0; i < this.restaurants.length; i++) {
        for(let j = 0; j < this.restaurants[i].foods.length; j++) {
          let food = this.restaurants[i].foods[j];

          food.quantity = 0;

          if(null != this.selections[i] && this.selections[i].length > 0) {
            for(let k = 0; k < this.selections[i].length; k++) {
              if(this.selections[i][k].name == food.name) {
                food.quantity = this.selections[i][k].quantity;
              }
            }
          }
          else {
            this.selections[i] = [];
          }
        }
      }
    });
  }

  openReviewOrder(i) {
    let pushObj = {
      "restaurant": this.restaurants[i],
      "restaurantIndex": i,
      "selections": this.selections
    };

    this.navCtrl.push(ReviewOrderPage, pushObj);
  }

}
