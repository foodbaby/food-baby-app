import { Component } from '@angular/core';
import {NavController, Tabs} from 'ionic-angular';

import * as Constants from '../../globals/constants';
import {Storage} from "@ionic/storage";

@Component({
  selector: 'page-select-menu',
  templateUrl: 'select-menu.html',
})
export class SelectMenuPage {

  public restaurants: any[] = Constants.TOUR.restaurants;
  public selections: any[] = [];

  public isExpanded: boolean[] = [];

  constructor(public navCtrl: NavController, private storage: Storage, private tabs: Tabs) {

    //this.isExpanded = [];

    for(let i = 0; i < this.restaurants.length; i++) {
      this.isExpanded[i] = (i == 0);
    }

    storage.get('foodSelections').then((val) => {
      if(null != val && val.length > 0) {
        this.selections = JSON.parse(val);
      }

      for(let i = 0; i < this.restaurants.length; i++) {
        for(let j = 0; j < this.restaurants[i].foods.length; j++) {
          let food = this.restaurants[i].foods[j];

          food.quantity = 0;

          if(null != this.selections[i]) {
            for(let k = 0; k < this.selections[i].length; k++) {
              if(this.selections[i][k].name == food.name) {
                food.quantity = this.selections[i][k].quantity;
              }
            }
          }
          else {
            this.selections[i] = [];
          }
        }
      }
    });
  }


  addSelection(food: any, restaurantIndex: number) {
    if(restaurantIndex < this.restaurants.length - 1) {
      food.quantity++;

      for(let i = 0; i < this.selections[restaurantIndex].length; i++) {
        if(this.selections[restaurantIndex][i].name == food.name) {
          this.selections[restaurantIndex].splice(i, 1);
          break;
        }
      }

      this.selections[restaurantIndex].push(food);

      this.saveSelections();
    }
  }

  removeSelection(food: any, restaurantIndex: number) {
    if(restaurantIndex < this.restaurants.length - 1) {
      for (let i = 0; i < this.selections[restaurantIndex].length; i++) {
        if (food.name == this.selections[restaurantIndex][i].name) {
          food.quantity--;

          if (food.quantity <= 0) {
            this.selections[restaurantIndex].splice(i, 1);
          }
          else {
            this.selections[restaurantIndex][i] = food;
          }

          break;
        }
      }

      this.saveSelections();
    }
  }

  saveSelections() {
    this.storage.set('foodSelections', JSON.stringify(this.selections));
  }

  goToStartTour() {
    this.tabs.select(2);
  }
}
