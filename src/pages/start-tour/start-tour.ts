import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import {SelectMenuPage} from "../select-menu/select-menu";
import * as Constants from "../../globals/constants";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import {HomePage} from "../home/home";
import {MoreInformationPage} from "../more-information/more-information";
import {NavigationModalPage} from "../navigation-modal/navigation-modal";
import {PayPage} from "../pay/pay";
import {VideoModalPage} from "../video-modal/video-modal";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'page-start-tour',
  templateUrl: 'start-tour.html'
})
export class StartTourPage{

  public selections: any[] = [];
  public tour: any = Constants.TOUR;

  constructor(public navCtrl: NavController, public launchNavigator: LaunchNavigator, public modalCtrl: ModalController,
              private storage: Storage) {
    storage.get('foodSelections').then((val) => {
      if (null != val && val.length > 0) {
        this.selections = JSON.parse(val);
      }

      for (let i = 0; i < this.tour.restaurants.length; i++) {
        for (let j = 0; j < this.tour.restaurants[i].foods.length; j++) {
          let food = this.tour.restaurants[i].foods[j];

          food.quantity = 0;

          if (null != this.selections[i]) {
            for (let k = 0; k < this.selections[i].length; k++) {
              if (this.selections[i][k].name == food.name) {
                food.quantity = this.selections[i][k].quantity;
              }
            }
          }
          else {
            this.selections[i] = [];
          }
        }
      }
    });
  }

  goToHome() {
    this.navCtrl.push(HomePage);
  }

  goToSelectMenu() {
    this.navCtrl.push(SelectMenuPage);
  }

  goToMoreInfo() {
    this.navCtrl.push(MoreInformationPage);
  }

  goToNavigationModal() {
    let modal = this.modalCtrl.create(NavigationModalPage, this.tour);
    modal.present();
  }

  goToPay() {
    this.navCtrl.push(PayPage);
  }

  goToVideoModal(restaurant) {
    let modal = this.modalCtrl.create(VideoModalPage, restaurant);
    modal.present();
  }

  getRouteString() {
    let returnString = "";

    for(let i = 0; i < this.tour.restaurants.length; i++) {
      let address = this.tour.restaurants[i].address;

      returnString += address.address1 + " " + address.locality + ", " + address.region + " " + address.zipcode;
      if(i < this.tour.restaurants.length - 1) {
        returnString += "+to:";
      }
    }

    return returnString;
  }

  openGoogleMaps() {
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS,
      transportMode: this.launchNavigator.TRANSPORT_MODE.WALKING
    };

    let routeString = this.getRouteString();

    this.launchNavigator.navigate(routeString, options)
      .then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
  }

  isSelectionsEmpty() {
    let empty = true;

    for(let i = 0; i < this.selections.length; i++) {
      for(let j = 0; j < this.selections[i].length; j++) {
        if(null != this.selections[i][j]) {
          empty = false;
          break;
        }
      }
    }

    return empty;
  }

}
