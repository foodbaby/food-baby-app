import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {LaunchNavigator, LaunchNavigatorOptions} from "@ionic-native/launch-navigator";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'page-video-modal',
  templateUrl: 'video-modal.html'
})
export class VideoModalPage {

  public video_url: string = this.navParams.get('video_url');

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              private launchNavigator: LaunchNavigator, private domSanitizer: DomSanitizer) {
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  getSafeVideoUrl() {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(this.video_url);
  }

}
