import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ComingSoonPage } from '../pages/coming-soon/coming-soon';
import { HomePage } from '../pages/home/home';
import { MoreInformationPage } from '../pages/more-information/more-information';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LaunchNavigator} from '@ionic-native/launch-navigator';
import {SelectMenuPage} from "../pages/select-menu/select-menu";
import {RecommendedMenuPage} from "../pages/recommended-menu/recommended-menu";
import {StartTourPage} from "../pages/start-tour/start-tour";
import {TourGalleryPage} from "../pages/tour-gallery/tour-gallery";
import {FoodModalPage} from "../pages/food-modal/food-modal";

import {RestaurantPage} from "../pages/restaurant/restaurant";
import {NavigationModalPage} from "../pages/navigation-modal/navigation-modal";
import {IonicStorageModule} from "@ionic/storage";
import {PayPage} from "../pages/pay/pay";
import {ReviewOrderPage} from "../pages/review-order/review-order";
import {VideoModalPage} from "../pages/video-modal/video-modal";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {CheckoutModalPage} from "../pages/checkout-modal/checkout-modal";

@NgModule({
  declarations: [
    MyApp,
    CheckoutModalPage,
    ComingSoonPage,
    FoodModalPage,
    HomePage,
    MoreInformationPage,
    NavigationModalPage,
    PayPage,
    RecommendedMenuPage,
    RestaurantPage,
    ReviewOrderPage,
    SelectMenuPage,
    StartTourPage,
    TabsPage,
    TourGalleryPage,
    VideoModalPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CheckoutModalPage,
    ComingSoonPage,
    FoodModalPage,
    HomePage,
    MoreInformationPage,
    NavigationModalPage,
    PayPage,
    RecommendedMenuPage,
    RestaurantPage,
    ReviewOrderPage,
    SelectMenuPage,
    StartTourPage,
    TabsPage,
    TourGalleryPage,
    VideoModalPage
  ],
  providers: [
    LaunchNavigator,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
