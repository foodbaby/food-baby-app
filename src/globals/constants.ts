const ADDRESSES = [
  {
    "address1": "117 Martin Luther King Jr Blvd.",
    "locality": "Madison",
    "region": "WI",
    "zipcode": 53703,
    "country": "United States",
    "latitude": 43.0736557,
    "longitude": -89.382477
  },
  {
    "address1": "111 S. Hamilton St.",
    "locality": "Madison",
    "region": "WI",
    "zipcode": 53703,
    "country": "United States",
    "latitude": 43.0726537,
    "longitude": -89.3840615
  },
  {
    "address1": "127 State St.",
    "locality": "Madison",
    "region": "WI",
    "zipcode": 53703,
    "country": "United States",
    "latitude": 43.0746263,
    "longitude": -89.3878174999999
  },
  {
    "address1": "320 State St.",
    "locality": "Madison",
    "region": "WI",
    "zipcode": 53703,
    "country": "United States",
    "latitude": 43.0749538,
    "longitude": -89.3907598
  },
  {
    "address1": "311 N. Frances St.",
    "locality": "Madison",
    "region": "WI",
    "zipcode": 53703,
    "country": "United States",
    "latitude": 43.0726686,
    "longitude": -89.3956259
  }
];

const FOODS = [
  [
    {
      "name": "Cheese Curds - 1/2 Order (v)",
      "description": "Deep fried Wisconsin cheddar, sea salt, and black pepper with buttermilk-dill dipping sauce.",
      "price": 4.79,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/curds.jpg"
    },
    {
      "name": "Fried Pickles - 1/2 Order (v)",
      "description": "Battered and fried pickles with house ranch sauce.",
      "price": 4.79,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/pickles.jpg"
    },
    {
      "name": "Curds + Pickles Combo (v)",
      "description": "A little of both. Perfect for sharing! ",
      "price": 7.19,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/sampler.jpg"
    },
    {
      "name": "Patio Pounder",
      "description": "Kegged tito's handmade vodka, sauvignon blanc-torrontes white wine blend, and fresh lime.",
      "price": 5.99,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/patiopounder.jpg"
    },
    {
      "name": "Double Fina Margarita",
      "description": "Kegged sauza blue reposado tequila, fresh lime, and blood orange purée.",
      "price": 5.99,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/doublefinamargarata.jpg"
    }
  ],
  [
    {
    "name": "Cauliflower Taco (v)",
    "description": "Tecate-battered cauliflower, guajillo chile sauce, crema, tomatillo-radish-red onion slaw, and cilantro.",
    "price": 4.19,
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/cauliflowertaco.jpg"
    },
    {
      "name": "Chile Chicken Taco",
      "description": "Roasted tomatillo and serrano salsa, crema, tomatillo-radish-red onion slaw, and cilantro.",
      "price": 3.59,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/chilechickentaco.jpg"
    },
    {
      "name": "Barbacoa Beef Brisket Taco",
      "description": "Arbol chile salsa, green cabbage, toasted seeds, crema, onion, and cilantro.",
      "price": 4.79,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/barbacoabeeftaco.jpg"
    },
    {
      "name": "Loaded Mexican Rice (v)",
      "description": "Rice, sofrito, black beans, corn, green onion, jalapeño, chihuahua cheese, cotija, cilantro.",
      "price": 4.79,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/loadedmexicanrice.jpg"
    },
    {
      "name": "House Margarita",
      "description": "Sauza blue reposado tequila, fresh-squeezed lime, patrón citronge, salt, on the rocks",
      "price": 4.19,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/housemargarita.jpg"
    }
  ],
  [
    {
    "name": "Premium Wine",
    "description": "5 oz. glass of select wines. ",
    "price": 5.99,
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/wine.jpg"
    },
    {
      "name": "Liqueur",
      "description": "3oz. glass of select liqueurs.",
      "price": 5.99,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/liqueur.jpg"
    },
    {
      "name": "Whisky",
      "description": "2oz. glass of select whiskys.",
      "price": 6.99,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/whisky.jpg"
    }
  ],
  [
    {
      "name": "Tasty Bowl",
      "description": "Salty and Crispy Popcorn Chicken, Pork Rib, Braised Pork over rice.",
      "price": 5.99,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/tastybowl.jpg"
    },
    {
      "name": "Flight of Milk Teas",
      "description": "Flight of 4 milk teas: Jasmine Green, Pouchong, Oolong, and Black.",
      "price": 3.99,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/milkteas.jpg"
    }
  ],
  [
    {
      "name": "Sasa Dango",
      "description": "Traditional sweet rice cake wrapped with bamboo leaves.",
      "price": 4.69,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/sasadango.jpg"
    },
    {
      "name": "Green Tea Mochi",
      "description": "Sweet rice dough filled with green tea ice cream.",
      "price": 4.69,
      "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/mochi.jpg"
    },
    {
      "name": "Red Bean Mochi",
      "description": "Sweet rice dough filled with red bean paste.",
      "price": 4.69,
      "image_url": ""
    }
  ]
];

const QUOTE = {
  "author": "John Paetsch",
  "image_url": "assets/imgs/avatar.png",
  "quote": "A lively crawl around the Capitol.",
  "title": "Local Guide"
};

const RESTAURANTS = [
  {
    "name": "DLUX",
    "description": "DLUX is a Capitol Square mainstay serving up posh burgers, otherworldly cheese curds, and boozy milkshakes. ",
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/DluxLogo.jpg",
    "facebook_url": "https://www.facebook.com/DluxMadison/",
    "instagram_url": "https://www.instagram.com/dluxmadison/",
    "video_url": "https://www.youtube.com/embed/CPVh38voQoc",
    "address": ADDRESSES[0],
    "foods": FOODS[0]
  },
  {
    "name": "Canteen",
    "description": "Innovating with the taco is a risky proposition, but the hip taqueria Canteen isn't afraid to experiment.",
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/CanteenLogo.jpg",
    "facebook_url": "https://www.facebook.com/canteentaco/?ref=br_rs",
    "instagram_url": "https://www.instagram.com/canteentaco/?hl=en",
    "video_url": "https://www.youtube.com/embed/XDJKrSqnizk",
    "address": ADDRESSES[1],
    "foods": FOODS[1]
  },
  {
    "name": "Vom Fass",
    "description": "\"See, taste, enjoy\" is Vom Fass' motto. Grab a beverage & wander around the store.",
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/VomFassLogo.jpg",
    "facebook_url": "https://www.facebook.com/vomfassmadisonstatestreet/",
    "instagram_url": "https://www.instagram.com/vomfass_statestreet/",
    "video_url": "https://www.youtube.com/embed/FyDeW4kZ7yY",
    "address": ADDRESSES[2],
    "foods": FOODS[2]
  },
  {
    "name": "Taiwan Little Eats",
    "description": "Straight 'outta Taipei, the chefs behind Taiwan Little Eats knows a thing or two about comfort food. ",
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/TLELogo.jpg",
    "facebook_url": "https://www.facebook.com/TWlileats/",
    "instagram_url": "https://www.instagram.com/taiwanlittleeats/",
    "video_url": "https://www.youtube.com/embed/li6QzG5UnXs",
    "address": ADDRESSES[3],
    "foods": FOODS[3]
  },
  {
    "name": "Strings Ramen",
    "description": "Ramen is king at Strings, but don't forget about their authentic Japanese desserts.",
    "image_url": "http://www.foodbabyapp.com/wp-content/uploads/2018/04/StringsLogo.jpg",
    "facebook_url": "https://www.facebook.com/StringsRamenShopMadison/",
    "instagram_url": "https://www.instagram.com/stringsramen/",
    "video_url": "https://www.youtube.com/embed/Ne93jS9fHCQ",
    "address": ADDRESSES[4],
    "foods": FOODS[4]
  }
];

const REVIEW = {
  "author": "John Paetsch",
  "image_url": "assets/imgs/avatar.png",
  "number": 10,
  "review": "Thank you for participating in the Early Access Event!" +
    " We hope you enjoy your first FoodBaby experience." +
    " <b>Look for us on the App Store this Summer!</b>" +
    "<br> <br> If you encounter any technical issues on the tour, please call (414) 530-9928",
  "title": "Local Guide"
};

export const TOUR = {
  "distance": "1.1 miles",
  "duration": "2.5 hrs.",
  "image_url": "assets/imgs/foodbaby-featured.jpg",
  "small_image_url": "assets/imgs/foodbaby-featured-small.jpg",
  "location": "Madison, WI",
  "name": "The Capitol Crawl",
  "price": 2,
  "quote": QUOTE,
  "rating": 10,
  "restaurants": RESTAURANTS,
  "review": REVIEW,
  "tags": ["#betatest", "#madison", "#wisconsin", "#capitol", "#American", "#Mexican", "#booze", "#margaritas", "#Asian"]
};

export const API_URL = "https://foodbaby-api.appspot.com";
export const IS_PROD = false;
export const STRIPE_APP_TOKEN = (IS_PROD ? "pk_live_P4l7J2egqoIpNYTSS8Ob5Q25" : "pk_test_eWnoxzGaj8CtzFW3QxHAMzfJ");
export const TF_APP_TOKEN = "fa11b096731d77f6d02cabdc8eac386a5f6aaa86";
